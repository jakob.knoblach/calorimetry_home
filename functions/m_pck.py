import os
import sys
import time
import json

from w1thermsensor import Sensor, W1ThermSensor
import h5py
import numpy as np


# This if statement enables you to write and run programs that test functions directly at the end of this file.
if __name__ == "__main__":
    import pathlib
    
    file_path = os.path.abspath(__file__)
    
    file_path = pathlib.Path(file_path)
    
    root = pathlib.Path(__file__).parent.parent
    
    sys.path.append(str(root))
#from functions import m_json


def check_sensors() -> None:
    #hier wird die Temperatur + Serial ausgelesen
    from w1thermsensor import W1ThermSensor, Sensor
    
    for sensor in W1ThermSensor.get_available_sensors([Sensor.DS18B20]):
        print("Sensor %s has temperature %.2f" % (sensor.id, sensor.get_temperature()))

   


def get_meas_data_calorimetry(metadata: dict) -> dict:
    """Collects and returns temperature measurement data from DS18B20 sensors based on the provided metadata.

    This function initializes sensor objects based on the metadata, prompts the user to start the
    measurement, and then continually reads temperature values until interrupted (e.g., via Ctrl-C).
    It logs the temperatures and the corresponding timestamps. Refer to README.md section
    "Runtime measurement data" for a detailed description of the output data structure.

    Args:
        metadata (dict): Contains details like sensor uuid, serials, and names.
                         Refer to README.md section "Runtime metadata" for a detailed description
                         of the input data structure.

    Returns:
        dict: A dictionary with sensor uuid as keys, and corresponding lists of temperatures and timestamps.

    Example:
        Input metadata:
        {
            "sensor": {
                "values": ["sensor_1", "sensor_2"],
                "serials": ["000005888445", "000005888446"],
                "names": ["FrontSensor", "BackSensor"]
            }
        }

        Output (example data after interruption):
        {
            "sensor_1": [[25.12, 25.15], [0, 2]],
            "sensor_2": [[24.89, 24.92], [0, 2]]
        }

    """
    # Initialize an empty dictionary for storing temperature measurements.
    # The structure is uuid: [[temperatures], [timestamps]].
    data = {i: [[], []] for i in metadata["sensor"]["values"]}
    start = time.time()
    sensor_list = ['3ce10457b355', '3ce10457d78f']
    input("Press any key to start measurement... <Ctrl+C> to stop measurement")
    try:
        while True:
            for i, sensor in enumerate(sensor_list):
                
                # TODO: Get experimental data.
                from w1thermsensor import W1ThermSensor, Sensor
                
                time_in_loop = time.time()
                usable_time = time_in_loop - start
             
                #hier wird der messzeitpunkt + dazugehöriger temperatur ausgelesen
                from w1thermsensor import W1ThermSensor, Sensor
                
                sensor = W1ThermSensor(sensor_type=Sensor.DS18B20,  sensor_id=sensor_list[i])
                temperature = sensor.get_temperature()
                
                if sensor_list[i] == '3ce10457b355':
                    data[metadata["sensor"]["values"][0]][0].append(temperature)
                    data[metadata["sensor"]["values"][0]][1].append(usable_time)
                elif sensor_list[i] == '3ce10457d78f':
                    data[metadata["sensor"]["values"][1]][0].append(temperature)
                    data[metadata["sensor"]["values"][1]][1].append(usable_time)
                
 
                time.sleep(6)
                    
           
                

                # DONE #
            # Print an empty line for better readability in the console.
            print("")
    # Catch the KeyboardInterrupt (e.g., from Ctrl-C) to stop the measurement loop.
    except KeyboardInterrupt:
        # Print the collected data in a formatted JSON structure.
        print(json.dumps(data, indent=4))
    # Always execute the following block.
    finally:
        # Ensure that the lengths of temperature and timestamp lists are the same for each sensor.
        for i in data:
            # If the temperature list is longer, truncate it to match the length of the timestamp list.
            if len(data[i][0]) > len(data[i][1]):
                data[i][0] = data[i][0][0 : len(data[i][1])]
            # If the timestamp list is longer, truncate it to match the length of the temperature list.
            elif len(data[i][0]) < len(data[i][1]):
                data[i][1] = data[i][1][0 : len(data[i][0])]

    return data


def logging_calorimetry(
    data: dict,
    metadata: dict,
    data_folder: str,
    json_folder: str,
) -> None:
    """Logs the calorimetry measurement data into an H5 file.

    This function creates a folder (if not already present) and an H5 file with a
    specific structure. The data from the provided dictionaries are written to the
    H5 file, along with several attributes.

    Args:
        data (dict): Contains sensor data including temperature and timestamp.
                     Refer to README.md section"Runtime measurement data" for a detailed
                     description of the data structure.
        metadata (dict): Contains metadata. Refer to README.md section "Runtime metadata"
                         for a detailed description of the structure.
        data_folder (str): Path to the folder where the H5 file should be created.
        json_folder (str): Path to the folder containing the datasheets.

    """
    # Extract the folder name from the provided path to be used as the H5 file name.
    log_name = data_folder.split("/")[-1]
    # Generate the full path for the H5 file.
    dataset_path = "{}/{}.h5".format(data_folder, log_name)
    # Check and create the logging folder if it doesn't exist.
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)

    # Create a new H5 file.
    f = h5py.File(dataset_path, "w")
    # Create a 'RawData' group inside the H5 file.
    grp_raw = f.create_group("RawData")

    # TODO: Add attribute to HDF5.
    # Set attributes for the H5 file based on the datasheets.
    f.attrs["created"] = "15.11.2023"
    f.attrs["experiment"] = "Kalorimetrie am Küchentisch"
    f.attrs["group_number"] = "29"
    f.attrs["authors"] = "Jakob Knoblach"
    #for i in ["created", "experiment", "group_number", "authors"]:
        #if f.attrs[i] is None:
            

    # DONE #

    # TODO: Write data to HDF5.
    #Hier werden die daten in ein HDF5 format umgewandelt
    group_sensor_1 = grp_raw.create_group("1ee7d4bf-3320-694f-bec8-88f2cd5b781a")
    group_sensor_2 = grp_raw.create_group("1ee7d4c0-eb4f-6204-b90b-7ecfb9fc03f9")
    group_sensor_1.attrs["name"] = "temperature_cup"
    group_sensor_1.attrs["serial"] = "3ce10457b355"
    group_sensor_2.attrs["name"] = "temperature_environment"
    group_sensor_2.attrs["serial"] = "3ce10457d78f"
    dsettemperature1 = group_sensor_1.create_dataset("temperature", data=data['1ee7d4bf-3320-694f-bec8-88f2cd5b781a'][0] )
    dsettimestamp1 = group_sensor_1.create_dataset("timestamp", data=data['1ee7d4bf-3320-694f-bec8-88f2cd5b781a'][1] )
    dsettemperature2 = group_sensor_2.create_dataset("temperature", data=data['1ee7d4c0-eb4f-6204-b90b-7ecfb9fc03f9'][0] )
    dsettimestamp2 = group_sensor_2.create_dataset("timestamp", data=data['1ee7d4c0-eb4f-6204-b90b-7ecfb9fc03f9'][1] )
    # DONE #

    # Close the H5 file.
    f.close()


if __name__ == "__main__":
    # Test and debug.
    pass
