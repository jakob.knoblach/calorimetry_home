from functions import m_json
from functions import m_pck

#Seriennummer und Temperatur von Sensor auslesen
m_pck.check_sensors()


#Hier wird das metadata der setup files erstellt
liste_of_setuppath=list()

liste_of_setuppath.append("datasheets/setup_heat_capacity.json")
liste_of_setuppath.append("datasheets/setup_newton.json")

metadata_of_setup=m_json.get_metadata_from_setup(liste_of_setuppath)

#Hier wird das metadata der setup files und der sensoren erstellt
liste_of_sensor_path=list()

liste_of_sensor_path.append("datasheets/sensor_1.json")
liste_of_sensor_path.append("datasheets/sensor_2.json")

real_metadata=m_json.add_temperature_sensor_serials(liste_of_sensor_path,metadata_of_setup)

#Hier wird die temperatur plus die zugehörigen zeitpunkte ausgelesen
timestamp_plus_temperature=m_pck.get_meas_data_calorimetry(real_metadata)


#hier werden die Daten als hdf5 abgespeichert (für probe newton)
m_pck.logging_calorimetry(timestamp_plus_temperature,real_metadata,'/home/pi/calorimetry_home/Versuch_newton','/home/pi/calorimetry_home/datasheets')

m_json.archiv_json('/home/pi/calorimetry_home/datasheets','/home/pi/calorimetry_home/datasheets/setup_newton.json','/home/pi/calorimetry_home/Versuch_newton')

#hier werden die Daten als hdf5 abgespeichert (für probe heat capacity)
#m_pck.logging_calorimetry(timestamp_plus_temperature,real_metadata,'/home/pi/calorimetry_home/Versuch_heat_capacity','/home/pi/calorimetry_home/datasheets')

#m_json.archiv_json('/home/pi/calorimetry_home/datasheets','/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json','/home/pi/calorimetry_home/Versuch_heat_capacity')
#für die beiden funktionen welche die Daten in hdf5 format abspeichern wird immer diejenige auskommentiert, die nicht für den Versuch benötigt wird